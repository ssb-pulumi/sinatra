import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import { Config, StackReference, Output } from "@pulumi/pulumi";
import { KUBE_CONFIG } from "./types";
import { App } from "./app";
import { IApp } from "./types";

export class Sinatra implements IApp {
  readonly name: string;
  readonly exposed: boolean;
  readonly replicas: number;
  readonly image: string;
  readonly containerPort: number;
  readonly serviceType: "ClusterIP" | "LoadBalancer";
  readonly environmentVars: { name: string; value: string }[] = [];

  constructor(tag: string) {
    this.name = "sinatra";
    this.exposed = true;
    this.replicas = 1;
    this.image = tag;
    this.containerPort = 1212;
    this.serviceType = "ClusterIP" as "ClusterIP" | "LoadBalancer";
    this.environmentVars = [{ name: "key", value: "sandeep" }];
  }
}

const config = new Config();
const sinatraImageTag = config.require("sinatra-image");

const providerRenderYaml = (app: string) =>
  new k8s.Provider("render-yaml", {
    renderYamlToDirectory: `${pulumi.getStack()}/${app}`,
  });

const provider = new k8s.Provider("k8s", { kubeconfig: KUBE_CONFIG });

// Kubernetes App(Rendered YAML and deployed through Arocd)
new App(new Sinatra(sinatraImageTag), providerRenderYaml("sinatra"));
