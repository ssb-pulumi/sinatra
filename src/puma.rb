#!/usr/bin/env puma
# frozen_string_literal: true

threads_count = ENV.fetch('PUMA_THREADS') { 5 }.to_i
threads threads_count, threads_count
port ENV.fetch('PORT') { 1212 }
workers ENV.fetch('WORKERS') { 1 }.to_i

# Load metrics plugin
plugin 'metrics'